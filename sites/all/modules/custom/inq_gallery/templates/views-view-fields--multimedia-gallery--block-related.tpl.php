<?php 
	if($fields['field_photo']){
		$photos = explode(",",$fields['field_photo']->content);
	}
    if($fields['field_video']){
    	$videos = explode(",",$fields['field_video']->content);
    }
?>
<div class="grid row">
	<?php if($fields['field_photo']): ?>
	    <?php foreach ($photos as $photo):
			//get the caption
        	$caption = strstr($photo, 'alt="');
        	$end = strpos($caption,'"',6);
        	$caption = substr($caption, 5, ($end - 5));]]
	    ?>

	    <div class="col-md-6 col-sm-12 box">
	    	<?php print $photo ; ?>
	        <div class="caption">
		        <p><?php print $caption ; ?></p>
		    </div>
	    </div>
	    <?php endforeach;?>
	 <?php endif;?>

	 <?php if($fields['field_video']): ?>
	    <?php foreach ($videos as $video): ?>
	    	<?php print $video ; ?>
	    <?php endforeach;?>
	 <?php endif;?>
</div>
<?php print $fields['link']->content?>

