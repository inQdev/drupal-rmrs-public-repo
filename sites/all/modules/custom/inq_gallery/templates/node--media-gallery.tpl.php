<?php 
	global $base_url;


	$photos = $content['field_photo']['#items'];
	$videos = $content['field_video']['#items'];
	$items = count($photos)> 0 | count($videos)> 0;
?>
<article class="node-<?php print $node->nid; ?> <?php print $classes; ?> clearfix margin-bottom-40"<?php print $attributes; ?>>
    <div class="document-extra-data">
		<?php print render($content['field_keywords']); ?>
	</div>
<?php if($items): ?>
	<div class="col-md-7">
		<div class="row">
		    <div id="grid" class="gallery">
				<?php if($photos): ?>
				    <?php foreach ($photos as $photo):
				        //get the caption
				        $caption = $photo['alt'];
				        $url = file_create_url($photo['uri']);

				        $image = array(
					      //'width' => '220',
		       			  //'height' => '220',
				          'style_name' => 'gallery',
					      'path' => file_create_url($url),
					      'class' => 'img-responsive',
					     );
			    	?>	
			    		<a href="<?php print $url ?>" rel="gallery" class="grid_lightbox" title="<?php print $caption ?>">
			    			<div class="box">
				    			<?php echo theme('image',$image) ?>
				    			<?php if(strlen($caption) > 0): ?>
					    			<span class="caption full-caption">
										<p><?php print $caption ; ?></p>
									</span>
								<?php endif; ?>
				    		</div>
			    		</a>
				    <?php endforeach; ?>
				 <?php endif;?>

				 <?php if($videos):?>
				 	<?php foreach ($videos as $video):
				 		  	//get the caption
					        $caption = $video['description'];

					        //In order to get better iframes, we need to get a cleaner URL
					        $url = $video['video_url'];

					        if (strpos($url,'youtube.com') !== false) {

						        parse_str(parse_url($url, PHP_URL_QUERY), $url);
						        $url = "http://www.youtube.com/embed/".$url['v']."?autoplay=1";

							}elseif(strpos($url,'vimeo.com') !== false) {
								$regexstr = '~
					            # Match Vimeo link and embed code
					            (?:<iframe [^>]*src=")?       # If iframe match up to first quote of src
					            (?:                         # Group vimeo url
					                https?:\/\/             # Either http or https
					                (?:[\w]+\.)*            # Optional subdomains
					                vimeo\.com              # Match vimeo.com
					                (?:[\/\w]*\/videos?)?   # Optional video sub directory this handles groups links also
					                \/                      # Slash before Id
					                ([0-9]+)                # $1: VIDEO_ID is numeric
					                [^\s]*                  # Not a space
					            )                           # End group
					            "?                          # Match end quote if part of src
					            (?:[^>]*></iframe>)?        # Match the end of the iframe
					            (?:<p>.*</p>)?              # Match any title information stuff
					            ~ix';
					 
					        	preg_match($regexstr, $url, $url);
				        		$url = "//player.vimeo.com/video/".$url[1]."?autoplay=1";
							}

					        $thumbnail = file_create_url($video['thumbnail_path']);

					         $image = array(
						      'width' => '220',
			       			  'height' => '220',
						      'path' => file_create_url($thumbnail),
						      'alt' => $caption,
						      'class' => 'img-responsive'
						     );
				 	?>	
				 		<a href="<?php print $url; ?>" class="grid_lightbox fancybox.iframe" rel="gallery" title="<?php print $caption ?>">
				 			<div class="box video-box">
			    				<?php echo theme('image',$image) ?>				    			
				    			<?php if(strlen($caption) > 0): ?>
					    			<span class="caption full-caption">
										<p><?php print $caption ; ?></p>
									</span>
								<?php endif; ?>
				    		</div>
				 		</a>
				 	<?php endforeach;?>
				 <?php endif;?>	

			</div>
		</div><!-- row -->
	</div><!-- col-md-7 -->
<?php endif; ?>
	
	<div class="col-md-<?php print ($items ? 5 : 12)?>">
		<?php 
			hide($content['field_keywords']);
			hide($content['field_photo']);
			hide($content['field_video']);
			hide($content['links']);
			hide($content['comments']);
			print render($content);
		 ?>
	</div>

	<div class="col-md-12">
		<?php if(!empty($content['comments'])): ?>
			<hr>
			<?php print render($content['comments']); ?>
		<?php endif; ?>
	</div>



</article>