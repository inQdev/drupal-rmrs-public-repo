<?php 
    $thumbnail = ( isset($fields['field_photo']->content) ? $fields['field_photo']->content : ( isset($fields['field_video']->content) ? $fields['field_video']->content : null));
?>

<div class="row  gallery margin-bottom-20">
	<?php if ($thumbnail): ?>
	    <div class="col-md-5">
	        <?php print $thumbnail ?>
	    </div>
	<?php endif; ?>
			<div class="col-md-<?php print ($thumbnail ? 7 : 12 ) ?>">
	        <h3><?php print $fields['title']->content ?></h3>
		    <ul class="list-unstyled list-inline blog-info"> 
		    	<li><?php print $fields['name']->label_html . $fields['name']->content ?></li>
		        <li><i class="fa fa-calendar"></i> <?php print  $fields['created']->content ?></li>
		        <li><i class="fa fa-comments"></i> <?php print  $fields['comment_count']->content ?></li>
		        <li><i class="fa fa-thumbs-up"></i> <?php print $fields['count']->content ?></li>
		    </ul>	
	        <?php print $fields['body']->content ?>
    	</div>    <!-- col-md-7 / col-md-12 -->
</div>
<hr class="margin-bottom-20">