jQuery(document).ready(function() {
  jQuery(".grid_lightbox").fancybox({
    maxWidth  : 800,
    maxHeight : 600,
    fitToView : false,
    width   : '70%',
    height    : '70%',
    autoSize  : false,
    closeClick  : false,
    openEffect  : 'none',
    closeEffect : 'none'
  });

  jQuery('.tooltip-hover').tipr({
    'mode': 'bottom'
  });

});