<?php
/**
 * @file
 * inq_media_galleries.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function inq_media_galleries_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function inq_media_galleries_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function inq_media_galleries_node_info() {
  $items = array(
    'inq_media_galleries' => array(
      'name' => t('Media Gallery'),
      'base' => 'node_content',
      'description' => t('Add images and videos to a Media Gallery'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
