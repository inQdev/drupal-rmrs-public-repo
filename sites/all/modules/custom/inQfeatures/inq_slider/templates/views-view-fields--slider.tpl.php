<div class="row slide">
	<div class="col-sm-5">
		<?php print $fields['field_slide_image']->content ?>
	</div>
	<div class="col-sm-7 slide-content">
		<header>
			<h3 class="slide-title"><?php print $fields['field_slide_title']->content ?></h3>
		</header>
		<?php print $fields['field_slide_description']->content ?>
		<footer>
			<div class="slide-link"><?php print $fields['field_slide_link']->content ?></div>
		</footer>
	</div>
</div>