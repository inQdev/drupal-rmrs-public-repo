<?php

/**
 * Page callback: Module's configuration page
 *
 */
function inq_rits_admin_form($form, &$form_state) {
  // The 'Days until Application becomes ineligible' configuration field.
  $form['inq_rits_test'] = array(
    '#type' => 'textfield',
    '#title' => t('Test.'),
    '#default_value' => ''/*variable_get('cied_cron_jobs_ineligible_days', 90)*/,
    '#size' => 3,
    '#maxlength' => 3,
    '#description' => t('Test.'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}