<?php

/**
 * Implements hook_drush_help().
 */
function inq_rits_drush_help($command) {
  switch ($command) {
    case 'drush:sync-rits':
      return dt('Sync content types to RITS.');
  }
}

/**
 * Implements hook_drush_command().
 */
function inq_rits_drush_command() {
  $items = array();
  $items['sync-rits'] = array(
    'description' => dt('Sync content types to RITS.'),
    'examples' => array(
      'Standard example' => 'drush sync-rits',
    ),
    'aliases' => array('sr'),
  );
  return $items;
}

/**
* Generates the applications report.
*/
function drush_inq_rits_sync_rits() {
  // sync_user_profiles();
  // sync_taxonomies();
  // sync_research_highlighs();
  sync_products_services();
}

// National Research Research Highlighs
function sync_products_services(){
  $products_services = get_entity_from_api('products_services');
  $row = 0;
  foreach ($products_services as $key => $products_service) {
    # code...
    if($key === 'products_service'){
      // if($row > 6700) {
      // Publications
      if((int)$products_service->product_type_id===1 && false){
        if(1) {
          // drush_print_r($products_service);
          $entity = entity_load_by_field_product_id((int)$products_service->product_id, 'publications');
          $ps_wrapper = entity_metadata_wrapper('node', (object)$entity);

          $ps_wrapper->author = user_load_by_name('rmrs-publisher');

          $ps_wrapper->field_product_id = (string)$products_service->product_id;

          $ps_wrapper->title = truncate_chars(strip_tags((string)$products_service->title), 252);
          $ps_wrapper->field_complete_title = strip_tags((string)$products_service->title);

          $authors = $products_service->authors->author;
          $ps_wrapper->field_internal_authors = array();
          $ps_wrapper->field_external_authors = array();
          foreach ($authors as $key2 => $author) {
            # code...
            if((string)$author->author_contact_id){
              $account = user_load_by_field_rits_user_id_or_username((int)$author->author_contact_id, (string)$author->alias);
              $ps_wrapper->field_internal_authors[] = $account->uid;
            }else{
              $ps_wrapper->field_external_authors[] = (string)$author->name;
            }
          }
          $year = (string)$products_service->product_year ? strtotime((string)$products_service->product_year.'-01-01') : null;
          $ps_wrapper->field_publication_year = $year;
          // $ps_wrapper->field_source = (string)$products_service->;
          $ps_wrapper->field_primary_station = (string)$products_service->primary_station;
          // $ps_wrapper->field_publication_series = (string)$products_service->;
          $ps_wrapper->field_abstract = (string)$products_service->abstract;
          // $ps_wrapper->field_url = (string)$products_service->pdf_url;
          // $ps_wrapper->field_download = (string)$products_service->;
          $ps_wrapper->field_citation = (string)$products_service->citation;

          $date = (string)$products_service->modified_on ? strtotime((string)$products_service->modified_on) : null;
          $ps_wrapper->field_date_modified = $date;

          $ps_wrapper->field_rits = array();
          $ps_taxonomies = $products_service->ps_taxonomies->ps_taxonomy;
          foreach ($ps_taxonomies as $key2 => $ps_taxonomy) {
            $ps_wrapper->field_rits[] = get_or_create_term((string)$ps_taxonomy->title, 'national_research');
          }

          $ps_wrapper->field_spa = array();
          $strategic_program_areas = $products_service->strategic_program_areas->strategic_program_area;
          foreach ($strategic_program_areas as $key2 => $strategic_program_area) {
            $ps_wrapper->field_spa[] = get_or_create_term((string)$strategic_program_area->spa_title, 'strategic_program_areas');
          }
          // $ps_wrapper->field_pra = (string)$products_service->;
          // $ps_wrapper->field_rmrs_science_program = (string)$products_service->;
          // $ps_wrapper->field_rmrs_strategic_priority = (string)$products_service->;
          // $ps_wrapper->field_geography = (string)$products_service->;

          $ps_wrapper->field_keywords = array();
          $keywords = explodeX(array(",",";"), strip_tags((string)$products_service->keywords));
          foreach ($keywords as $key2 => $keyword) {
            $ps_wrapper->field_keywords[] = get_or_create_term($keyword, 'keywords');
          }

          $ps_wrapper->field_entry_status = trim((string)$products_service->entry_status);
          $ps_wrapper->field_treesearch_url = array('value' => trim((string)$products_service->treesearch_url));
          $ps_wrapper->field_pdf_url = array('value' => trim((string)$products_service->pdf_url));
          $ps_wrapper->field_pub_parent_id = (int)$products_service->pub_parent_id;
          $ps_wrapper->field_pub_child_number = (int)$products_service->pub_child_number;
          $ps_wrapper->field_pub_parent_ind = (int)$products_service->pub_parent_ind;
          $ps_wrapper->field_doi = trim((string)$products_service->doi);
          $ps_wrapper->field_proceedings_ind = (int)$products_service->proceedings_ind;


          // drush_print_r($ps_wrapper->value());
          // Save research highligh
          $rh = $ps_wrapper->save();
          $node = node_load($rh->nid->value());
          // drush_print_r($node);

          $created_on = trim((string)$products_service->created_on) ? strtotime(trim((string)$products_service->created_on)) : null;
          // drush_print_r("modified_on $modified_on");
          // $edit = array(
          //   'created' => $created_on,
          // );
          // node_save($node, $edit);
          if ($node = node_submit($node)) {
            $node->created = $created_on;
            node_save($node);
          } else {
              drupal_set_message(t("Node ".$node->title." added incorrectly"), "error");
          }
          print_r("Product $row inserted or updated\n");
        }
        $row++;
      }
      // Tools
      if((int)$products_service->product_type_id===2 && (int)$products_service->product_subtype_id != 23){
        if (1) {
          // drush_print_r($products_service);
          $entity = entity_load_by_field_product_id((int)$products_service->product_id, 'tools');
          $ps_wrapper = entity_metadata_wrapper('node', (object)$entity);

          $ps_wrapper->author = user_load_by_name('rmrs-publisher');

          $ps_wrapper->field_product_id = (string)$products_service->product_id;

          $ps_wrapper->title = trim((string)$products_service->title);
          $ps_wrapper->field_citation = array('value' => trim((string)$products_service->citation), 'format' => 'full_html');
          $ps_wrapper->field_overview = array('value' => trim((string)$products_service->abstract), 'format' => 'full_html');

          $authors = $products_service->authors->author;
          $ps_wrapper->field_internal_tool_developers = array();
          $ps_wrapper->field_external_tool_developers = array();
          foreach ($authors as $key2 => $author) {
            # code...
            if((string)$author->author_contact_id){
              $account = user_load_by_field_rits_user_id_or_username((int)$author->author_contact_id, (string)$author->alias);
              $ps_wrapper->field_internal_tool_developers[] = $account->uid;
            }else{
              $ps_wrapper->field_external_tool_developers[] = (string)$author->name;
            }
          }

          $ps_wrapper->field_website = array('value' => trim((string)$products_service->product_location));

          $ps_wrapper->field_rits = array();
          $ps_taxonomies = $products_service->ps_taxonomies->ps_taxonomy;
          foreach ($ps_taxonomies as $key2 => $ps_taxonomy) {
            $ps_wrapper->field_rits[] = get_or_create_term((string)$ps_taxonomy->title, 'national_research');
          }

          $ps_wrapper->field_spa = array();
          $strategic_program_areas = $products_service->strategic_program_areas->strategic_program_area;
          foreach ($strategic_program_areas as $key2 => $strategic_program_area) {
            $ps_wrapper->field_spa[] = get_or_create_term((string)$strategic_program_area->spa_title, 'strategic_program_areas');
          }

          $ps_wrapper->field_keywords = array();
          $keywords = explodeX(array(",",";"), strip_tags((string)$products_service->keywords));
          foreach ($keywords as $key2 => $keyword) {
            $keyword = trim($keyword);
            $ps_wrapper->field_keywords[] = get_or_create_term($keyword, 'keywords');
          }

          $ps_wrapper->field_product_sub_type = trim((string)$products_service->product_subtype_desc);
          $ps_wrapper->field_product_sub_type_id = trim((string)$products_service->product_subtype_id);

          $date = (string)$products_service->modified_on ? strtotime((string)$products_service->modified_on) : null;
          $ps_wrapper->field_modified_on = $date;

          // drush_print_r($ps_wrapper->value());
          // Save research highligh
          $rh = $ps_wrapper->save();
          $node = node_load($rh->nid->value());
          // drush_print_r($node);

          $created_on = trim((string)$products_service->created_on) ? strtotime(trim((string)$products_service->created_on)) : null;
          // drush_print_r("modified_on $modified_on");
          // $edit = array(
          //   'created' => $created_on,
          // );
          // node_save($node, $edit);
          if ($node = node_submit($node)) {
            $node->created = $created_on;
            node_save($node);
          } else {
              drupal_set_message(t("Node ".$node->title." added incorrectly"), "error");
          }
          print_r("Tools $row inserted or updated\n");
        }
        $row++;
      }
    }
  }
}

// National Research Research Highlighs
function sync_research_highlighs(){
  $research_highlights = get_entity_from_api('research_highlights');
  $row = 0;
  foreach ($research_highlights as $key => $research_highlight) {
    # code...
    // if($research_highlight->high_id == 543){
    if($key === 'research_highlight'){
      if(1){
      // if($row<10){
        $entity = entity_load_by_field_high_id((int)$research_highlight->high_id, 'research_highlights');
        $rh_wrapper = entity_metadata_wrapper('node', (object)$entity);

        $rh_wrapper->author = user_load_by_name('rmrs-publisher');

        $rh_wrapper->field_high_id = trim((string)$research_highlight->high_id);

        $rh_wrapper->title = trim((string)$research_highlight->title);
        $rh_wrapper->field_sub_title = trim((string)$research_highlight->sub_title);
        $rh_wrapper->field_highlight_summary = array(
          'value' =>
          (trim((string)$research_highlight->summary_para_1) ? '<p>' . trim((string)$research_highlight->summary_para_1) . '</p>' : '') .
          (trim((string)$research_highlight->summary_para_2) ? '<p>' . trim((string)$research_highlight->summary_para_2) . '</p>' : '') .
          (trim((string)$research_highlight->summary_para_3) ? '<p>' . trim((string)$research_highlight->summary_para_3) . '</p>' : '') .
          (trim((string)$research_highlight->summary_para_4) ? '<p>' . trim((string)$research_highlight->summary_para_4) . '</p>' : '') .
          (trim((string)$research_highlight->summary_para_5) ? '<p>' . trim((string)$research_highlight->summary_para_5) . '</p>' : '') .
          (trim((string)$research_highlight->summary_para_6) ? '<p>' . trim((string)$research_highlight->summary_para_6) . '</p>' : ''),
          'summary' => trim((string)$research_highlight->teaser),
          'format' => 'full_html'
        );
        // $rh_wrapper->field_media_gallery = trim((string)$research_highlight->);
        // $rh_wrapper->field_document_asset_ = trim((string)$research_highlight->);

        $rh_wrapper->field_principal_investigator = array();
        $authors = $research_highlight->principal_investigators->principal_investigator;
        foreach ($authors as $key2 => $author) {
          # code...
          if((string)$author->alias){
            $account = user_load_by_name((string)$author->alias);
            $rh_wrapper->field_principal_investigator[] = $account->uid;
          }
        }

        $rh_wrapper->field_photo_filename = array('value' => trim((string)$research_highlight->photo_filename));
        $rh_wrapper->field_photo_caption = trim((string)$research_highlight->photo_caption);
        $rh_wrapper->field_forest_service_partners = trim((string)$research_highlight->forest_service_partners);
        $rh_wrapper->field_external_partners = trim((string)$research_highlight->external_partners);
        // $rh_wrapper->field_publications = trim((string)$research_highlight->);
        $rh_wrapper->field_research_location = trim((string)$research_highlight->research_location);

        $year = trim((string)$research_highlight->fiscal_year) ? strtotime(trim((string)$research_highlight->fiscal_year).'-01-01') : null;
        $rh_wrapper->field_fiscal_year = $year;
        // $rh_wrapper->field_rits = trim((string)$research_highlight->);

        $rh_wrapper->field_spa = array();
        $strategic_program_areas = $research_highlight->strategic_program_areas->strategic_program_area;
        foreach ($strategic_program_areas as $key2 => $strategic_program_area) {
          $rh_wrapper->field_spa[] = get_or_create_term(trim((string)$strategic_program_area->spa_title), 'strategic_program_areas');
        }

        // $rh_wrapper->field_pra = trim((string)$research_highlight->);
        // $rh_wrapper->field_rmrs_science_program = trim((string)$research_highlight->);
        // $rh_wrapper->field_rmrs_strategic_priority = trim((string)$research_highlight->);
        // $rh_wrapper->field_geography = trim((string)$research_highlight->);
        // $rh_wrapper->field_keywords = trim((string)$research_highlight->);

        $modified_on = trim((string)$research_highlight->modified_on) ? strtotime(trim((string)$research_highlight->modified_on)) : null;
        $rh_wrapper->field_modified_on = $modified_on;


        // drush_print_r($rh_wrapper->value());
        // Save research highligh
        $rh = $rh_wrapper->save();
        $node = node_load($rh->nid->value());
        // drush_print_r($node);

        $created_on = trim((string)$research_highlight->created_on) ? strtotime(trim((string)$research_highlight->created_on)) : null;
        // drush_print_r("created_on $created_on");
        // $edit = array(
        //   'created' => $created_on,
        // );
        if ($node = node_submit($node)) {
            $node->created = $created_on;
            node_save($node);
        } else {
            drupal_set_message(t("Node ".$node->title." added incorrectly"), "error");
        }
        // node_save($node, $edit);

        print_r("Research highlight $row inserted or updated\n");
      }
      $row++;
    }
  }
}

// National Research Taxonomy
function sync_taxonomies(){
  $taxonomies = get_entity_from_api('taxonomy');

  $row = 0;
  print_r("Migrating $taxonomies->total_records Taxonomies\n");
  foreach ($taxonomies as $key => $taxonomy) {
    # code...
    if($key === 'taxonomy'){
      $parent = get_or_create_term((string)$taxonomy->level1_title,'national_research');
      if((string)$taxonomy->level2_title){
        get_or_create_term((string)$taxonomy->level2_title,'national_research', $parent->tid);
      }
      print_r("Taxonomy $row inserted or updated\n");
      $row++;
    }
  }
}
function sync_user_profiles(){
  $user_profiles = get_entity_from_api('user_profiles');

  $row = 0;
  print_r("Migrating $user_profiles->total_records User profiles\n");
  foreach ($user_profiles as $key => $userProfile) {
    // if((int)$userProfile->user_id === 1728){
    if($key === 'userProfile'){
      if(1){
      // if(1){
        // drush_print_r($userProfile);
        $account = user_load_by_field_rits_user_id_or_username((int)$userProfile->user_id, (string)$userProfile->alias);
        $usr_wrapper = entity_metadata_wrapper('user', (object)$account);

        // Writting user data

        $usr_wrapper->field_rits_user_id = trim((string)$userProfile->user_id);
        // $usr_wrapper->name = trim((string)$userProfile->alias);
        // Changed on 2014-12-05
        $usr_wrapper->name = trim((string)$userProfile->author_contact_alias);
        $usr_wrapper->mail = trim((string)$userProfile->email_address);
        $usr_wrapper->roles[] = 8;

        $usr_wrapper->field_first_name = trim((string)$userProfile->firstname);
        $usr_wrapper->field_last_name = trim((string)$userProfile->lastname);
        $usr_wrapper->field_title = trim((string)$userProfile->job_title);
        $usr_wrapper->field_address = array(
          "country" => 'US',
          "locality" => trim((string)$userProfile->city),
          "administrative_area" => trim((string)$userProfile->state_code),
        );
        $usr_wrapper->field_phone = trim((string)$userProfile->phone);
        $usr_wrapper->field_current_research = array('value' => trim((string)$userProfile->bio_current_research), 'format' =>'full_html');
        $usr_wrapper->field_research_interests = array('value' => trim((string)$userProfile->bio_research_interest), 'format' =>'full_html');
        $usr_wrapper->field_past_research = array('value' => trim((string)$userProfile->bio_past_research), 'format' =>'full_html');
        $usr_wrapper->field_why_this_research_is_impor = array('value' => trim((string)$userProfile->bio_importance), 'format' =>'full_html');

        $usr_wrapper->field_education = null;
        $educations = $userProfile->educations->education;
        foreach ($educations as $key2 => $education) {
          $field_education = create_field_collection('field_education', $usr_wrapper);
          $field_education->field_institution = trim((string)$education->institution);
          $field_education->field_degree = trim((string)$education->degree);
          $year = trim((string)$education->year) ? strtotime(trim((string)$education->year).'-01-01') : null;
          $field_education->field_year = $year;
          $field_education->field_education_order = trim((string)$education->education_order);
          $field_education->field_area_of_study = trim((string)$education->area_of_study);
        }
        // $usr_wrapper->field_publications = trim((string)$userProfile->);
        // $usr_wrapper->field_featured_external_publicat = trim((string)$userProfile->);
        // $usr_wrapper->field_research_highlights = trim((string)$userProfile->);
        // $usr_wrapper->field_research_lab = trim((string)$userProfile->);
        // $usr_wrapper->field_google_scholar_profile = trim((string)$userProfile->);
        // $usr_wrapper->field_research_gate_profile = trim((string)$userProfile->);

        $usr_wrapper->field_rits = array();
        $profile_taxonomies = $userProfile->profile_taxonomies->profile_taxonomy;
        foreach ($profile_taxonomies as $key2 => $profile_taxonomy) {
          $usr_wrapper->field_rits[] = get_or_create_term(trim((string)$profile_taxonomy->title), 'national_research');
        }

        $usr_wrapper->field_spa = array();
        $strategic_program_areas = $userProfile->strategic_program_areas->strategic_program_area;
        foreach ($strategic_program_areas as $key2 => $strategic_program_area) {
          $usr_wrapper->field_spa[] = get_or_create_term(trim((string)$strategic_program_area->spa_title), 'strategic_program_areas');
        }

        $usr_wrapper->field_pra = array();
        $emerging_research_areas = $userProfile->emerging_research_areas->emerging_research_area;
        foreach ($emerging_research_areas as $key2 => $emerging_research_area) {
          $usr_wrapper->field_pra[] = get_or_create_term(trim((string)$emerging_research_area->era_title), 'priority_research_area');
        }

        $usr_wrapper->field_rmrs_science_program = array();
        $station_specific_sciences = $userProfile->station_specific_sciences->station_specific_science;
        foreach ($station_specific_sciences as $key2 => $station_specific_science) {
          $usr_wrapper->field_rmrs_science_program[] = get_or_create_term(trim((string)$station_specific_science->theme_description), 'rmrs_science_program_area');
        }

        // $usr_wrapper->field_rmrs_strategic_priority = trim((string)$userProfile->);
        // $usr_wrapper->field_geography = trim((string)$userProfile->);
        // $usr_wrapper->field_keywords = trim((string)$userProfile->);
        // $usr_wrapper->field_linkedin = trim((string)$userProfile->);
        // $usr_wrapper->field_external_links = trim((string)$userProfile->);
        // $usr_wrapper->field_suplementary_information = trim((string)$userProfile->);

        $usr_wrapper->field_full_name = trim((string)$userProfile->fullname);
        $middle_initial = trim((string)$userProfile->middle_initial);
        $usr_wrapper->field_middle_initial = $middle_initial . ($middle_initial ? '.' : '');
        $usr_wrapper->field_suffix = trim((string)$userProfile->suffix);
        $usr_wrapper->field_fax = trim((string)$userProfile->fax);
        $usr_wrapper->field_picture_file_name = array('value' => trim((string)$userProfile->picture_file_name));
        $usr_wrapper->field_picture_file_caption = trim((string)$userProfile->picture_file_caption);
        $usr_wrapper->field_resume_file_name = array('value' => trim((string)$userProfile->resume_file_name));

        $usr_wrapper->field_professional_organizations = null;
        $professional_organizations = $userProfile->professional_organizations->organization;
        foreach ($professional_organizations as $key2 => $organization) {
          $field_organization = create_field_collection('field_professional_organizations', $usr_wrapper);
          $field_organization->field_organization_order = (string)trim($organization->organization_order);

          $year_start = trim((string)$organization->year_start) ? trim((string)$organization->year_start).'-01-01' : null;
          $year_end = trim((string)$organization->year_end) && trim((string)$organization->year_end) != 'Current' ? trim((string)$organization->year_end).'-01-01' : null;
          $field_organization->field_years = array('value' => $year_start, 'value2' => $year_end);

          $field_organization->field_committee_membership = (string)trim($organization->committee_membership);
          $field_organization->field_involvement_summary = (string)trim($organization->involvement_summary);
          $field_organization->field_organization_name = (string)trim($organization->organization_name);
          $field_organization->field_organization_role_name = (string)trim($organization->organization_role_name);
        }

        $usr_wrapper->field_professional_experiences = null;
        $field_professional_experiences = $userProfile->professional_experiences->experience;
        foreach ($field_professional_experiences as $key2 => $experience) {
          $field_experience = create_field_collection('field_professional_experiences', $usr_wrapper);
          $field_experience->field_experience_order = (string)trim($experience->experience_order);

          $year_start = trim((string)$experience->year_start) ? trim((string)$experience->year_start).'-01-01' : null;
          $year_end = trim((string)$experience->year_end) && trim((string)$experience->year_end) != 'Current' ? trim((string)$experience->year_end).'-01-01' : null;
          $field_experience->field_years = array('value' => $year_start, 'value2' => $year_end);

          $field_experience->field_title = (string)trim($experience->title);
          $field_experience->field_institution = (string)trim($experience->institution);
          $field_experience->field_narrative = (string)trim($experience->narrative);
        }

        $usr_wrapper->field_awards = null;
        $awards = $userProfile->awards->award;
        foreach ($awards as $key2 => $award) {
          $field_award = create_field_collection('field_awards', $usr_wrapper);

          $field_award->field_education_order = trim((string)$award->education_order);
          $year = trim((string)$award->year) ? strtotime(trim((string)$award->year).'-01-01') : null;
          $field_award->field_year = $year;
          $field_award->field_award_type = trim((string)$award->award_type);
          $field_award->field_major_minor = trim((string)$award->major_minor);
          $field_award->field_long_description = trim((string)$award->description);
        }

        $modified_on = trim((string)$userProfile->modified_on) ? strtotime(trim((string)$userProfile->modified_on)) : null;
        $usr_wrapper->field_modified_on = $modified_on;

        $approved_on = trim((string)$userProfile->approved_on) ? strtotime(trim((string)$userProfile->approved_on)) : null;
        $usr_wrapper->field_approved_on = $approved_on;

        $created_on = trim((string)$userProfile->created_on) ? strtotime(trim((string)$userProfile->created_on)) : null;

        // Activate User
        $usr_wrapper->status = trim((string)$userProfile->active_inactive);
        // $usr_wrapper->status = $usr_wrapper->status->value() ? $usr_wrapper->status->value() : 0;

        // Save user
        $usr = $usr_wrapper->save();
        $account = user_load($usr->uid->value());
        // drush_print_r($account);
        $edit = array(
          'pass' => 'RMRS2014!',
          'created' => $created_on,
        );
        user_save($account, $edit);

        print_r("User $row inserted or updated\n");
      }
      $row++;
    }
  }
}

function get_entity_from_api($entity){
  global $base_url;
  $api_base_url = $base_url . '/rits/';

  $entity_url = $api_base_url . 'xml_' . $entity . '.xml';

  return simplexml_load_file($entity_url);
}

function user_load_by_field_rits_user_id_or_username($value, $username=''){
  $entity = db_query('SELECT n.* FROM {users} AS n LEFT JOIN {field_data_field_rits_user_id} AS e ON e.entity_id = n.uid where field_rits_user_id_value = :value',array('value' => $value));
  $entity = $entity->fetchAll();
  if(!empty($entity)){
     return reset($entity);
  }else{
    return user_load_by_name($username);
  }
}

function entity_load_by_field_high_id($value, $type){
  $entity = db_query('SELECT n.* FROM {node} AS n LEFT JOIN {field_data_field_high_id} AS e ON e.entity_id = n.nid where field_high_id_value = :value',array('value' => $value));
  $entity = $entity->fetchAll();
  if(!empty($entity)){
     return reset($entity);
  }else{
    $entity = entity_create('node', array('type' => $type));
    // Specify the author
    return $entity;
  }
}

function entity_load_by_field_product_id($value, $type){
  $entity = db_query('SELECT n.* FROM {node} AS n LEFT JOIN {field_data_field_product_id} AS e ON e.entity_id = n.nid where field_product_id_value = :value',array('value' => $value));
  $entity = $entity->fetchAll();
  if(!empty($entity)){
     return reset($entity);
  }else{
    $entity = entity_create('node', array('type' => $type));
    // Specify the author
    return $entity;
  }
}

function create_field_collection($field_name, &$wrapper, $is_array = false){
  $field_collection = array('field_name' => $field_name, 'host_entity' => $wrapper);
  $field_collection = entity_property_values_create_entity('field_collection_item', $field_collection);
  //if($is_array){
    return $field_collection;
  //}
}


function get_or_create_term($term_name, $vocabulary, $parent = 0){
  if(empty($term_name)) return null;
  $term = taxonomy_get_term_by_name($term_name, $vocabulary);
  if(empty($term)){
    $newterm = new stdClass();
    $newterm->name = $term_name;
    $newterm->vid = taxonomy_vocabulary_machine_name_load($vocabulary)->vid;
    $newterm->parent = $parent;
    taxonomy_term_save($newterm);
    $term = taxonomy_get_term_by_name($term_name, $vocabulary);
  }
  // drush_print_r(end($term));
  return end($term);
}

function truncate_chars($text, $limit, $ellipsis = '...') {
    if( strlen($text) > $limit )
        $text = trim(substr($text, 0, $limit)) . $ellipsis;
    return $text;
}

function explodeX($delimiters,$string)
{

  $return_array = Array($string); // The array to return
  $d_count = 0;
  while (isset($delimiters[$d_count])) // Loop to loop through all delimiters
  {
    $new_return_array = Array();

    foreach($return_array as $el_to_split) // Explode all returned elements by the next delimiter
    {
      $put_in_new_return_array = explode($delimiters[$d_count],$el_to_split);
      foreach($put_in_new_return_array as $substr) // Put all the exploded elements in array to return
      {
        $new_return_array[] = $substr;
      }
    }
    $return_array = $new_return_array; // Replace the previous return array by the next version
    $d_count++;
  }
  return $return_array; // Return the exploded elements
}