<?php

/**
 * Implements hook_drush_help().
 */
function inq_save_all_nodes_again_drush_help($command) {
  switch ($command) {
    case 'drush:save-all-nodes':
      return dt('Save all nodes again.');
  }
}

/**
 * Implements hook_drush_command().
 */
function inq_save_all_nodes_again_drush_command() {
  $items = array();
  $items['save-all-nodes'] = array(
    'description' => dt('Save all nodes again.'),
    'examples' => array(
      'Standard example' => 'drush san',
    ),
    'aliases' => array('san'),
  );
  return $items;
}

/**
* Generates the applications report.
*/
function drush_inq_save_all_nodes_again_save_all_nodes() {
  $nids = db_query('SELECT nid FROM {node}')->fetchCol();
  $row = 0;
  drush_print_r($nids);
  foreach ($nids as $nid) {
    if($row > 7760){
      $node = node_load($nid);
      node_save($node);
      drush_print_r("Row $row saved");
    }
    $row++;
  }
}