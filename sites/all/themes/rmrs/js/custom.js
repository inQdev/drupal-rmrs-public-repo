//Custom js

function buildTagCloud($cloudContainer) {
	var links = $cloudContainer.find('a'),
		max   = 0;

	// find the max, so we can normalize
	jQuery(links).each(function(){
		var current = jQuery(this).attr("data-count");
		if (current > max) { max = current; }
	});

	jQuery(links).each(function(){
		var freq = jQuery(this).attr("data-count"),
			pct  = (freq.valueOf() / max) * 100, //normalized value
			size;
		
		if (pct < 20) size = "x-small";
		else if (pct < 40) size = "small";
		else if (pct < 60) size = "medium";
		else if (pct < 80) size = "large";
		else size = "x-large";
		
		jQuery(this).addClass(size);
	});
}

jQuery(document).ready(function($){
	// Keyboard Tag Cloud
	var $cloudContainer = $('.view-keywords-cloud');
	if ($cloudContainer.length !== 0) {
		buildTagCloud($cloudContainer);
	}

	/*// Fancy boxs
	var images_links = $('.field-collection-item-field-photos .field-item a');
	images_links.fancybox({
		arrows: true,
		nextClick: true,
		type: 'image',
	});*/

});

jQuery(window).load(function(){
	// Sets content wrapper height to use full height of the screen
	if(jQuery('.page-wrapper').length > 0){
		var windowHeight = jQuery(window).height(); 
		var headerHeight = jQuery('header').height()-jQuery('.main-navigation').height();
		var footerHeight = jQuery('.page-footer').height() + jQuery('.page-footer').outerHeight();

		jQuery('.page-wrapper').css('min-height', windowHeight-headerHeight-footerHeight);
	}
});


jQuery(function(){

	if(jQuery('.home-search-form').length > 0){

		var searchForm = jQuery('.home-search-form');

		//Gets form fields
		jQuery(searchForm).find('.container-inline').removeClass('container-inline');
		jQuery(searchForm).find('.form-type-textfield').addClass('col-md-5 clearfix');
		jQuery(searchForm).find('.form-type-select').addClass('col-md-5 clearfix');

	}

	if(jQuery('.search-open').length > 0){

		var searchForm = jQuery('.search-open');

		//Gets form fields
		jQuery(searchForm).find('.container-inline').removeClass('container-inline');
		jQuery(searchForm).find('.form-type-textfield').removeClass('form-item').addClass('col-md-5 clearfix');
		jQuery(searchForm).find('.form-type-select').removeClass('form-item').addClass('col-md-5 clearfix');

	}


	jQuery('.megamenu').hover(function(){
		jQuery('.search.fa.search-btn').removeClass('fa-times').addClass('fa-search');
		jQuery('.search-open').stop().fadeOut();
	});

});