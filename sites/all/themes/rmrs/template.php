<?php

function rmrs_breadcrumb($variables) {

    $output = FALSE;

    $breadcrumb = $variables['breadcrumb'];

    if (!empty($breadcrumb)) {

        $breadcrumb[] = drupal_get_title();

        $output = '<ul class="breadcrumb">';

        foreach ($breadcrumb as $b) {
        	$output .= '<li>'.$b.'</li>';
        }

        $output .= '</ul>';

        return $output;
    }
}

/**
 * Implements hook_js_alter
 * Unset default jQuery so we can add the newest versions
 * @param type $js
 */
function rmrs_js_alter(&$js) {
    unset($js['misc/jquery.js']);
    unset($js['misc/ui/jquery.ui.core.min.js']);
}

/**
 * Implements hook_menu_tree (for user menu only)
 * @global type $level
 * @param type $variables
 * @return type
 */
function rmrs_menu_tree__user_menu($variables) {
	return '<ul class="list-unstyled top-v1-data">' . $variables['tree'] . '</ul>';
}

/**
 * Implemements hook_process_page
 * @param array $variables
 */
function rmrs_preprocess_page(&$variables) {
    
    //Search form
    $search_form = drupal_get_form('search_form');
    $search_form_box = drupal_render($search_form);
    $variables['search_box'] = $search_form_box;

    if ( $views_page = views_get_page_view() ) {
        $variables['theme_hook_suggestions'][] = 'page__views__' . $views_page->name;
        $variables['theme_hook_suggestions'][] = 'page__views__' . $views_page->name . '_' . $views_page->current_display;
    }

    if (isset($variables['node']->type)) {
        $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
    }
    
    // changes the links tree for the main menu, since by
    // default Drupal just provides first level ones
    $menu_tree = menu_tree_all_data('main-menu');
    $menu_tree = menu_tree_output($menu_tree);

    foreach ($menu_tree	as $k=>$v) {
        if (is_numeric($k) && count($v['#below'])) {
            $menu_tree[$k]['#below']['#theme_wrappers'][0] = 'menu_tree__submenu';
        }
    }    
    
    $variables['main_menu'] = $menu_tree;   
    
    if(arg(0) == 'taxonomy' && arg(1) == 'term') {
        $tid = (int)arg(2);
        $term = taxonomy_term_load($tid);
        if(is_object($term)) {
            $variables['theme_hook_suggestions'][] = 'page__taxonomy__'.$term->vocabulary_machine_name;
        }
    }  

}

function rmrs_menu_tree__main_menu($variables) {
    return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}

function rmrs_menu_tree__submenu($variables) {
    return '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
}

function rmrs_menu_link__main_menu($variables) {

    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function rmrs_menu_local_tasks(&$variables) {
    $output = '';

    if (!empty($variables['primary'])) {
        $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
        $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs primary">';
        $variables['primary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['primary']);
    }
    if (!empty($variables['secondary'])) {
        $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
        $variables['secondary']['#prefix'] .= '<ul class="nav nav-tabs tabs secondary">';
        $variables['secondary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['secondary']);
    }

    return $output;
}

/**
* Implements template_preprocess_node()
*/
function rmrs_preprocess_node(&$variables, $hook) {

  /*
  * Search for node type specific functions
  */
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }

   /*
  * Adding theme suggestions
  */

  $view_mode = $variables['view_mode'];
  $content_type = $variables['type'];
  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode;
  $variables['theme_hook_suggestions'][] = 'node__' . $view_mode . '__' . $content_type;

}

function rmrs_preprocess_views_view_table(&$vars) {
    $vars['classes_array'][] = 'table';
}



function rmrs_item_list($variables) {
    $items = $variables['items'];
    $title = $variables['title'];
    $type = $variables['type'];
    $attributes = $variables['attributes'];

    // Only output the list container and title, if there are any list items.
    // Check to see whether the block title exists before adding a header.
    // Empty headers are not semantic and present accessibility challenges.
    $output = '';
    if (isset($title) && $title !== '') {
        $output .= '<h3>' . $title . '</h3>';
    }

    if (!empty($items)) {
        $output .= "<$type" . drupal_attributes($attributes) . '>';
        $num_items = count($items);
        $i = 0;
        foreach ($items as $item) {
            $attributes = array();
            $children = array();
            $data = '';
            $i++;
            if (is_array($item)) {
                foreach ($item as $key => $value) {
                    if ($key == 'data') {
                        $data = $value;
                    }
                    elseif ($key == 'children') {
                        $children = $value;
                    }
                    else {
                        $attributes[$key] = $value;
                    }
                }
            }
            else {
                $data = $item;
            }
            if (count($children) > 0) {
                // Render nested list.
                $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
            }
            if ($i == 1) {
                $attributes['class'][] = 'first';
            }
            if ($i == $num_items) {
                $attributes['class'][] = 'last';
            }
            $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
        }
        $output .= "</$type>";
    }
    return $output;
}


function rmrs_links__system_main_menu($variables) {

    $to_render = drupal_get_form('search_block_form');
    $search_box = drupal_render($to_render);   
    
    $pieces = explode('/', drupal_get_path_alias());
    $path = '';
    $parent_candidates = array();
    
    foreach ($pieces as $piece) {
        $path .= $piece . '/';
        $parent_candidates[] = drupal_get_normal_path(rtrim($path, '/'));
    }    

    $html = '<ul class="nav navbar-nav">';

    $class = array();

    $megamenu = get_megamenu_regions();

    foreach ($variables['links'] as $key => $link) {
        
        if(is_array($link) ) {
        
            if(array_key_exists('#attributes', $link) ) {
                $class = $link['#attributes']['class'];

                //Adds the dropdown class if the expanded class is found
                $expanded = array_search("expanded", $class);
                if(strlen($expanded)>0){
                    $class[$expanded] .= " dropdown";
                }
            }

            if( array_key_exists('#href', $link) ) {
                if(in_array($link['#href'], $parent_candidates)) {
                    $class[] = 'active-trail';
                }
            }

            //Check if the current menu item has a megamenu
            $megamenu_string = "";
            if (array_key_exists($key, $megamenu)) {
                $megamenu_string .= "<div class='megamenu-items container'>";

                    $num_columns = count($megamenu[$key]);

                    foreach ($megamenu[$key] as $megakey => $block) {

                        $megamenu_string .= '<div class="col-md-4">';
                        $megamenu_string .= drupal_render($block);
                        $megamenu_string .= "</div>";
                        
                    }
                $megamenu_string .= "</div><!-- megamenu container -->";
                array_push($class, "megamenu");
            }

            //Builds the submenu string
            /*$submenu_string = "";
            if( array_key_exists('#below', $link) ) {
                if(count($link['#below']) > 0){
                    
                    $submenu_links = $link['#below'];
                    $submenu_string = '<ul class="nav navbar-nav">';

                    foreach ($submenu_links as $skey => $submenu_link) {
                        if( $skey != '#sorted' && $skey != '#theme_wrappers' ) {

                            $sub_menu_class = $submenu_link['#attributes']['class'];

                            //Gets current item classes 
                            $submenu_string .= '<li '.drupal_attributes(array('class' => $sub_menu_class)).'>' . l($submenu_link['#title'], $submenu_link['#href'], $submenu_link) . '</li>';
                        }
                    }
                    $submenu_string .= '</ul>';
                }
            }*/

            if( $key != '#sorted' && $key != '#theme_wrappers' ) {
                //$html .= '<li' . drupal_attributes(array('class' => $class)) . '>' . l($link['#title'], $link['#href'], $link) .$submenu_string. '</li>';
                $html .= '<li' . drupal_attributes(array('class' => $class)) . '>' . l($link['#title'], $link['#href'], $link) .$megamenu_string. '</li>';
            }             
            
        }
            
    }

    if(!drupal_is_front_page()){

        $html .= '<li class="pull-right menu-search">
                <i class="search fa fa-search search-btn"></i>
                <div class="search-open container">
                    <div class="input-group animated fadeInDown">
                        <div class="col-xs-12">'.$search_box.'</div>
                        <!--<input type="text" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button class="btn-u" type="button">Go</button>
                        </span>-->
                    </div>
                </div>    
            </li>';

    }

    $html .= '  </ul>';
    return $html;

}


/*** Drupal region on menu item
** This function search for regions that matches menu ID's. In order to add or modify regions refer to rmrs.info
**/
function get_megamenu_regions(){
    
    /* Menu ID collection
    * 218 = HOME
    * 869 = About RMRS
    * 712 = Projects
    * 896 = Research Highlights
    * 659 = Documents
    */

    // Get a list of all the regions for this theme
    $mega_menu_items = array();
    foreach (system_region_list($GLOBALS['theme']) as $region_key => $region_name) {

        // Get only regions with the megamenu prefix
        if (strpos($region_key,'megamenu') !== false) {
            // Validates if the current region has blocks
            if ($blocks = block_get_blocks_by_region($region_key)) {
                
                // Get assigned menu item
                $assigned_menu = explode("_", $region_key);

                // Get blocks and stores the results in an array [menu item id] [region position] [region blocks]

                $mega_menu_items[$assigned_menu[1]][$assigned_menu[2]] = $blocks;

            }
        }
    }

    return $mega_menu_items;

}


function rmrs_form_search_block_form_alter(&$form, $form_state, $form_id) {
    
    //Add custom classes to search form
    $form['search_block_form']['#field_prefix'] = '<div class="row">';
    $form['search_block_form']['#attributes']['class'][] = 'form-control';
    $form['search_block_form']['#field_suffix'] = '</div>';
    
    $form['custom_search_types']['#field_prefix'] = '<div class="row">';
    $form['custom_search_types']['#attributes']['class'][] = 'form-control';
    $form['custom_search_types']['#field_suffix'] = '</div>';

    $form['actions']['#attributes']['class'][] = 'col-md-2 clearfix'; 

    $form['actions']['submit']['#prefix'] = '<div class="row">';
    $form['actions']['submit']['#attributes']['class'][] = 'btn btn-primary pull-left'; 
    $form['actions']['submit']['#suffix'] = '</div>';
    
    //Add a placeholder to the text input
    $form['search_block_form']['#attributes']['placeholder'] = 'Search RMRS';

}

function rmrs_form_alter(&$form, &$form_state, $form_id) {

    
    // Login Form
    if( $form_id == 'user_login_block' || $form_id == 'user_login' ) {

        $form['name']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-user"></i></span>';
        $form['name']['#field_suffix'] = '</div>'; 
        $form['name']['#description'] = '';
        $form['name']['#attributes']['class'][] = 'form-control';

        $form['pass']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-lock"></i></span>';
        $form['pass']['#field_suffix'] = '</div>'; 
        $form['pass']['#description'] = '';
        $form['pass']['#attributes']['class'][] = 'form-control';

        $form['links']['#markup'] = '<div class="note"><a href="/user/password" title="Request a new password">'.t('Forgot password?').'</a></div>';

        $form['actions']['submit']['#attributes']['class'][] = 'btn-u btn-success pull-right';

    }

    if( $form_id == 'user_pass' ) {

        $form['name']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-user"></i></span>';
        $form['name']['#field_suffix'] = '</div>';
        $form['name']['#description'] = 'You will receive an email notification with more instructions to create a new password.';
        $form['actions']['submit']['#attributes']['class'][] = 'btn-u btn-success';
    }

    if( $form_id == 'user_register_form' ) {
        $form['actions']['submit']['#value'] = t('Register');
    }

    if(isset($form['select']['submit'])){
        $form['select']['submit']['#attributes']['class'][] = 'btn-primary';
    }
}


// REF: http://cubiq.org/the-perfect-php-clean-url-generator
// setlocale(LC_ALL, 'en_US.UTF8');
function inq_slugify($str, $replace=array(), $delimiter='-') {
    if( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}


function rmrs_fancybox_imagefield($variables) {
  $image = $variables['image'];
  $path = $variables['path'];
  $caption = $variables['caption'];
  $group = $variables['group'];

  return l(theme(empty($image['style_name']) ? 'image' : 'image_style', $image), $path, array(
    'html' => TRUE,
    'attributes' => array(
      'title' => $caption,
      'class' => 'fancybox',
      'data-fancybox-group' => 'gallery',
    ),
  ));
}