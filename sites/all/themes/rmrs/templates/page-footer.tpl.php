<?php if($page['footer'] || $page['copyright']): ?>

<footer id="footer" class="<?php print $classes; ?> footer page-footer">    
    <div class="container">
        <div class="row">
            <?php if($page['copyright']): ?>
                <div class="col-md-6 clearfix copyright">
                    <p class="pull-left"><?php echo date("Y"); ?> &copy; All rights reserved</p>
                    <div class="pull-left"><?php print render($page['copyright']); ?></div>
                </div>
            <?php endif; ?>

            <?php if($page['footer']): ?>
                <div class="col-md-6 text-right clearfix">
                    <?php print render($page['footer']); ?>
                </div>
            <?php else: ?>
                <ul class="col-md-6 follow-us text-right list-inline clearfix">
                    <li>Follow Us:</li>
                    <li>
                        <ul class="social-icons">
                            <li><a href="https://www.facebook.com/USDA" target="_blank" data-original-title="Facebook" class="rounded-x social_facebook"></a></li>
                            <li><a href="http://twitter.com/#!/usfs_rmrs/" target="_blank" data-original-title="Twitter" class="rounded-x social_twitter"></a></li>
                            <li><a href="http://www.youtube.com/user/usdaForestService" target="_blank" data-original-title="YouTube" class="rounded-x social_youtube"></a></li>
                        </ul>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</footer>

<?php endif; ?>