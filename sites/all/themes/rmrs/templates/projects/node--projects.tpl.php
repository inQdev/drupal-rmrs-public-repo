<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($display_submitted): ?>
    <div class="submitted">
      <?php print $submitted; ?>
    </div>
  <?php endif; ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
    ?>
    <?php if ($page): ?>
      <hr>
        <div class="center">
          <div class="col-sm-4">
            <?php
              print render($content["field_project_status"]);
            ?>
          </div>
          <div class="col-sm-4">
            <?php
              print render($content["field_project_dates"]);
            ?>
          </div>
          <div class="col-sm-4">
            <?php
              print render($content["field_project_location"]);
            ?>
          </div>
        </div>
      <div class="clearfix"></div>
      <hr>

      <!-- Tabs -->
      <div class="tab-v2">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Description</a></li>
          <li role="presentation"><a href="#gallery" aria-controls="gallery" role="tab" data-toggle="tab">Gallery</a></li>
          <li role="presentation"><a href="#additional-resources" aria-controls="additional-resources" role="tab" data-toggle="tab">Additional Resources</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content clearfix">
          <div role="tabpanel" class="tab-pane active" id="description">
              <div class="col-md-9">
                <?php
                  print render($content["field_project_description"]);
                ?>
                <?php
                  print render($content["field_background_and_scope"]);
                  print render($content["field_process_and_implementation"]);
                  print render($content["field_project_outcomes"]);
                  print render($content["field_project_summary"]);
                  print render($content["field_project_deliverables"]);
                  print render($content["field_learning_objectives"]);
                  print render($content["field_other"]);
                ?>
              </div>
              <div class="col-md-3">
                <?php print render($content["field_principal_investigators"]); ?>
                <?php print render($content["field_collaborators"]); ?>
                <?php print render($content["field_project_contact"]); ?>
                <?php print render($content['field_internal_rmrs_staff']) ?>
                  <?php print render($content['field_external_rmrs_individuals']) ?>
              </div>
          </div>
          <div role="tabpanel" class="tab-pane" id="gallery">
            <?php
              print render($content["field_media_gallery"]);
            ?>
          </div>
          <div role="tabpanel" class="tab-pane" id="additional-resources">
            <div>
              <?php print render($content["field_related_events"]); ?>
            </div>
            <hr>
            <div class="clearfix"></div>
            <div class="col-sm-6">
              <?php print render($content["field_publications"]); ?>
            </div>
            <div class="col-sm-6">
              <?php print render($content["field_related_documents_project"]); ?>
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="col-sm-12">
              <?php print render($content["field_related_projects"]); ?>
            </div>
          </div>
        </div>

      </div>
      <!-- End Tabs -->
      <hr>
    <?php else: ?>
        <?php
          print render($content);
        ?>
    <?php endif ?>



  </div>
  <?php #print render($content['links']); ?>

  <?php #print render($content['comments']); ?>

</div>
