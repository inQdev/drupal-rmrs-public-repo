<?php 
    $thumbnail = ( isset($fields['field_photo']->content) ? $fields['field_photo']->content : ( isset($fields['field_video']->content) ? $fields['field_video']->content : null));
?>
<h3><?php print $fields['title']->content ?></h3>	     
<div class="row">
	<?php if ($thumbnail): ?>
	    <div class="col-sm-5">
	        <?php print $thumbnail ?>
	    </div>
	<?php endif; ?>
	<div class="col-sm-<?php print ($thumbnail ? 7 : 12 ) ?>">
	     
	     <?php if(isset($fields['body']->content)){
	     	print $fields['body']->content;
	     	}
	     ?>
	</div>
</div>