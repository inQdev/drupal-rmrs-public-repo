<div class="servive-block servive-block-default viewgroups-block">
	<div class="row">
			<?php print $fields['title']->content ?>
			<div class="magazine-posts-img group-list-imgs">
				<?php print $fields['field_thumbnail']->content ?>
		    </div>		
			<div class="group-list-description">
				<div class="field-content">
					<?php print $fields['field_members']->content ?> <?php print $fields['field_members']->label ?>
					<?php print $fields['field_external_members']->content ?> <?php print $fields['field_external_members']->label ?>			
				</div>
			</div>
	</div>
</div>