<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

	<h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>

	<div class="content"<?php print $content_attributes; ?>>

		<?php
	      hide($content['comments']);
	      hide($content['links']);
	      hide($content['field_thumbnail']);
	      hide($content['field_members']);
	      hide($content['field_external_members']);
	      hide($content['field_related_documents']);
	      hide($content['field_publications']);
	      hide($content['field_primary_poc']);
	    ?>

		<div class="row">
			<div class="col-md-9 col-sm-12">
				<?php
	              print render($content['field_description_summary']);
	            ?>
			</div>
			<div class="col-md-3 col-sm-12">
				<?php
	              print render($content['field_thumbnail']);
	              print render($content['field_primary_poc']);
	            ?>
			</div>
		</div>

		<section id="group_content" class="col-md-12">
                 <?php if($page['group_latest']): print render($page['group_latest']);  endif; ?>
        </section>   
	</div>
</div>