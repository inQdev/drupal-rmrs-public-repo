<?php foreach ($rows as $id => $row): ?>
    <?php $str = explode('"', $row); ?>

    <li class="<?php print $classes_array[$id]; ?>"><?php print $row; ?>
        <ul class="child-menu">
        	<li class="child-menu-item"><a href="<?php print $str[1] ?>/research-highlights">Research Highlights</a></li>
            <li class="child-menu-item"><a href="<?php print $str[1] ?>/publications">Publications</a></li>
            <li class="child-menu-item"><a href="<?php print $str[1] ?>/members">Members</a></li>
            <li class="child-menu-item"><a href="<?php print $str[1] ?>/events">Events</a></li>
    	</ul>
    </li>
<?php endforeach; ?>