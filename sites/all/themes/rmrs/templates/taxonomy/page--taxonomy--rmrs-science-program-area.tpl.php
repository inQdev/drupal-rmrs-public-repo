<?php include_once( path_to_theme() . '/templates/page-header.tpl.php' ); ?>

<div class="wrapper page-wrapper">

    <div class="container content">
            
            <?php if($page['sidebar_left']): print render($page['sidebar_left']);  endif; ?>
            
            <div id="content" class="col-md-<?php if($page['sidebar_left']){ print '9';} else { print '12';} ?>" role="main">
                    
                <?php if ($messages): ?>
                <div id="messages" class="row">
                    <div class="col-xs-12">
                        <?php print $messages; ?>
                    </div>
                </div>
                <?php endif; ?>
                

                <?php print render($title_prefix); ?>
                <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                <?php print render($title_suffix); ?>

                <?php print print_tabs($tabs, $page, $action_links); ?>
                
                <?php if ($breadcrumb && !$is_front): ?>
                <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                <?php endif; ?>

                <div class="magazine-page row">
                    
                    <?php if($page['tax_slider']): ?>
                    <section id="taxonomy_slider" class="col-md-12">
                        <?php print render($page['tax_slider']); ?>
                    </section>
                    <?php endif; ?>
                    
                    <section id="taxonomy_content" class="col-md-12">
                        <?php if($page['tax_latest']): print render($page['tax_latest']);  endif; ?>
                    </section>           
                    
                    <?php if($page['below_content']): print render($page['below_content']);  endif; ?>
                    
                </div>
                
            </div>
            
    </div>
</div>
<?php include_once( path_to_theme() . '/templates/page-footer.tpl.php' ); ?>