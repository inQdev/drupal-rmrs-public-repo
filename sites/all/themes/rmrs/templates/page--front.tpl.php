<?php include_once( path_to_theme() . '/templates/page-header.tpl.php' ); ?>

<div class="wrapper page-wrapper">

    <div class="container content home-container">
            <div id="content" class="col-md-12" role="main">
                    
                <?php if ($messages): ?>
                <div id="messages" class="row">
                    <div class="col-xs-12">
                        <?php print $messages; ?>
                    </div>
                </div>
                <?php endif; ?>


                <div class="magazine-page">

                    <?php if($page['front_main']): print render($page['front_main']);  endif; ?>
                    
                    <div class="row">
                        <?php if($page['front_first']): print render($page['front_first']);  endif; ?>
                    </div>

                </div>
                
            </div>
        
    </div>
</div>

<?php include_once( path_to_theme() . '/templates/page-footer.tpl.php' ); ?>