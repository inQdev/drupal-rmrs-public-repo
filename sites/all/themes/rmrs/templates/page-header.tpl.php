<header class="header header-v1">
    
    <div class="topbar topbar-v1 margin-bottom-10">
        <div class="container">
            <div class="row">

                <?php
                    /**
                     * Top left branding layout
                     */
                ?>                
                <div class="col-md-5">
                    <div class="usda-branding-top">
                        
                        <div class="row first-row">
                            <img class="usdalogo" alt="USDA Logo" src="/<?php print drupal_get_path('theme',$GLOBALS['theme']); ?>/img/usdalogo.png" />
                            <img class="usfslogo" alt="Forest Service Shield" src="/<?php print drupal_get_path('theme',$GLOBALS['theme']); ?>/img/usfslogo.png" />
                            <h2>U.S. Forest Service <br><small>Caring for the land and serving people</small></h2>
                        </div>
                        <div class="row second-row">
                            <p>United States Department of Agriculture</p>
                        </div>

                    </div>
                </div>
                
                <?php
                    /**
                     * By default, her e it loads the search form
                     * to override this just add blocks to Header top right region
                     */
                ?>                  

                <div class="col-md-7 hidden-xs text-right">
                    <div class="row">
                        <h2><b><span class="text-green">Rocky Mountain</span> Research Station</b></h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="navbar navbar-default main-navigation" role="navigation">
        <div class="container">
            <div class="row">  
                
                <?php
                    /**
                     * By default, here it loads the main menu (if any),
                     * to override this just add blocks to Header nav region
                     */
                ?>
                
                <?php if($page['header_nav']): ?>
                    <div class="navbar-header">
                        <div class="col-xs-10">
                            <h2 class="pull-left visible-xs-block hidden-sm hidden-md hidden-lg"><b>Rocky Mountain Research Station</b></h2>
                        </div>
                        <div class="col-xs-2">
                            <div class="row">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="fa fa-bars"></span>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="main-navigation-collapse">
                        <?php //print render($page['header_nav']);  ?>
                        
                        <?php if ($main_menu): ?>
                            <?php print theme('links__system_main_menu', array('links'=>$main_menu)) ?>
                        <?php endif; ?>
                        
                    </div>
                <?php else: ?>
                
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="fa fa-bars"></span>
                        </button>
                    </div>

                    <?php if ($main_menu): ?>

                        <?php 
                            print theme('links__system_main_menu', array('links' => $main_menu));
                        ?>
                     <?php endif; ?>
                
                <?php endif; ?>
            </div>                
        </div>
    </div>
</header>