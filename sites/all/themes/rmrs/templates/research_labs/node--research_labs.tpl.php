<?php
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <?php print $user_picture; ?>

  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php
      hide($content['comments']);
      hide($content['links']);
      hide($content['field_people']);
      hide($content['field_events']);
      hide($content['field_address']);
      hide($content['field_keyword']);
      hide($content['field_rits']);
      hide($content['field_pra']);
      hide($content['field_spa']);
    ?>
    <div class="row">
      <div class="col-sm-6">
        <h3>Contact</h3>
      	<?php print render($content["field_address"]); ?>
      </div>
      <div class="col-sm-6">
        <?php if($content["field_voice"]): ?>
          <h3>Voice</h3>
          <?php print render($content["field_voice"]); ?>
        <?php endif; ?>
        <?php if($content["field_fax"]): ?>
          <h3>Fax</h3>
          <?php print render($content["field_fax"]); ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="row">
    	<div class="col-sm-9">
        <h3>Overview</h3>
    		<?php print render($content["field_description_summary"]); ?>
    	</div>
    </div>
    <div class="row">
    	<!-- Tabs -->
      	<div class="tab-v2">

	        <!-- Nav tabs -->
	        <ul class="nav nav-tabs" role="tablist">
	          <li role="presentation" class="active"><a href="#researchers" aria-controls="researchers" role="tab" data-toggle="tab">Researchers</a></li>
	          <li role="presentation"><a href="#events" aria-controls="events" role="tab" data-toggle="tab">Events</a></li>
	        </ul>
	        <!-- Tab panes -->

	        <!-- Tab panes -->
	        <div class="tab-content clearfix">
	          <div role="tabpanel" class="tab-pane active" id="researchers">
              <?php print views_embed_view('people_related','block', $node->nid); ?>
	          </div>
	          <div role="tabpanel" class="tab-pane" id="events">
	          	<?php print views_embed_view('events','related_events', $node->nid); ?>
	          </div>
	        </div>
	    </div>

    </div>
  </div>