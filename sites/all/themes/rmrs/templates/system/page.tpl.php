<?php include_once( path_to_theme() . '/templates/page-header.tpl.php' ); ?>

<div class="wrapper page-wrapper">

    <div class="container content">
            
            <?php if($page['sidebar_left']): print render($page['sidebar_left']);  endif; ?>
            
            <div id="content" class="col-md-<?php if($page['sidebar_left'] && $page['sidebar_right']){ print '6';} elseif($page['sidebar_left'] || $page['sidebar_right']){ print '9';} else { print '12';} ?>" role="main">
                    
                <?php if ($messages): ?>
                <div id="messages" class="row">
                    <div class="col-xs-12">
                        <?php print $messages; ?>
                    </div>
                </div>
                <?php endif; ?>
                

                <?php print render($title_prefix); ?>
                <?php if ($title): ?><h1 class="title" id="page-title"><?php print $title; ?></h1><?php endif; ?>
                <?php print render($title_suffix); ?>

                <?php print print_tabs($tabs, $page, $action_links); ?>
                
                <?php if ($breadcrumb && !$is_front): ?>
                <div id="breadcrumb"><?php print $breadcrumb; ?></div>
                <?php endif; ?>

                <div class="magazine-page">
                    
                    <?php if($page['above_content']): print render($page['above_content']);  endif; ?>

                    <?php print render($page['content']); ?>
                    
                    <?php if($page['below_content']): print render($page['below_content']);  endif; ?>
                    
                </div>
                
            </div>
            
            <?php if($page['sidebar_right']): print render($page['sidebar_right']);  endif; ?>
            
    </div>
</div>
<?php include_once( path_to_theme() . '/templates/page-footer.tpl.php' ); ?>