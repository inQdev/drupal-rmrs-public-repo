1. Use the "childtheme" folder as a startpoint, put that folder
out, at the same level as "inqbase" theme.

2. Rename the folder, use only letters and underscores

3. Use that same name (of the folder) to rename the .info file (i.e. name_of_the_file.info)

4. Open the .info file and change the name of the theme to the one you created

5. Go to Drupal admin interface and enable both, parent and child theme, but set child as default

6. Use custom.css and custom.js to write your code

7. To override the templates you can copy/paste the .tpl.php files and change them (se the same folder structure /templates )