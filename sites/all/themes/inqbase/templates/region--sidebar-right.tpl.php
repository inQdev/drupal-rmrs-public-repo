<?php if($content): ?>
<aside id="sidebar-right" class="sidebar col-md-3 <?php print $classes; ?>">
    <?php print $content ?>
</aside>
<?php endif; ?>
