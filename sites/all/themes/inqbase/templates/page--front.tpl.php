<header class="header header-v1">
    
    <div class="topbar topbar-v1 margin-bottom-10">
        <div class="container">
            <div class="row">
                
                <?php
                    /**
                     * By default, here it loads the logo (if any)
                     * to override this just add blocks to Header top left region
                     */
                ?>                
                
                <?php if($page['header_top_left']): print render($page['header_top_left']);  else: ?>
                <div class="col-md-6">
                    <?php if ($logo): ?>
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
                        <img id="logo-header" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                    </a>
                    <?php endif; ?> 
                </div>
                <?php endif; ?>
                
                
                <?php
                    /**
                     * By default, her e it loads the search form
                     * to override this just add blocks to Header top right region
                     */
                ?>                  
                <?php if($page['header_top_right']): print render($page['header_top_right']);  else: ?>
                    <?php print $search_box ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="row">  
                
                <?php
                    /**
                     * By default, here it loads the main menu (if any),
                     * to override this just add blocks to Header nav region
                     */
                ?>
                
                <?php if($page['header_nav']): print render($page['header_nav']);  else: ?>
                
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="fa fa-bars"></span>
                    </button>
                </div>

                <?php if ($main_menu): print theme('links__system_main_menu', array('links' => $main_menu)); endif;?>
                
                <?php endif; ?>
            </div>                
        </div>
    </div>
</header>

<div class="wrapper">

    <div class="container content">
        <div class="row">
            <div id="content" class="col-md-<?php if ($page['front_sidebar']){ print '9';} else { print '12';} ?>" role="main">
                    
                <?php if ($messages): ?>
                <div id="messages" class="row">
                    <div class="col-xs-12">
                        <?php print $messages; ?>
                    </div>
                </div>
                <?php endif; ?>

                <?php print print_tabs($tabs, $page, $action_links); ?>

                <div class="magazine-page">

                    <?php if($page['front_main']): print render($page['front_main']);  endif; ?>
                    
                    <?php if($page['front_first']): print render($page['front_first']);  endif; ?> 
                    
                    <?php if($page['front_second']): print render($page['front_second']);  endif; ?>
                    
                </div>
                
            </div>
            
            <?php if($page['front_sidebar']): print render($page['front_sidebar']);  endif; ?>
            
        </div>
    </div>
</div>

<?php if($page['footer']): ?>
<footer id="footer" class="<?php print $classes; ?> footer">	
    <div class="container">
        <div class="row">
            <?php print render($page['footer']); ?>
        </div>  	   
    </div>  		
</footer>  
<?php endif; ?>

<?php if($page['copyright']): ?>
<div class="copyright">
    <div class="container">
        <div class="row">
            <?php print render($page['copyright']); ?>
        </div>
    </div> 
</div>
<?php endif; ?>