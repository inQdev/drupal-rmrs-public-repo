
<?php
// Gets the Login form block and renders it according to our needs
// Links (bottom element) were added in the ascrs_form_user_login_block_alter
$elements = drupal_get_form('user_login_block');
?>
<form action="<?php echo $elements['#action']; ?>" method="<?php echo $elements['#method']; ?>" id="<?php echo $elements['#id']; ?>" class="smart-form reg-page">
    <div class="reg-header">            
        <h2>Account Login</h2>
    </div>
    
    <?php echo drupal_render($elements['name']); ?>
                         
    <?php echo drupal_render($elements['pass']); ?>
                      

    <div class="row">
        <div class="col-md-12">
            <?php echo drupal_render($elements['form_build_id']); ?>
            <?php echo drupal_render($elements['form_id']); ?>
            <?php echo drupal_render($elements['actions']); ?>                       
        </div>
    </div>

    <hr>

    <p><?php echo drupal_render($elements['links']); ?></p>
</form>