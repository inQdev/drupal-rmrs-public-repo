<!DOCTYPE html>
<!--[if IE 8]> <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"> <!--<![endif]-->
<head>
    <title><?php print $head_title; ?></title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <?php print $head; ?>

    <?php print $styles; ?>
    
    <?php /* jQuery and jQuery Ui loaded at top to avoid Js errors */ ?>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
</head>

<body class="<?php print $classes; ?>" <?php print $attributes;?>>

    <?php print $page_top; ?>

    <?php print $page; ?>
    
    <?php print $scripts; ?>

    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
        });
    </script>
    <!--[if lt IE 9]>
        <script src="/<?php print drupal_get_path('theme', 'cop') ?>/js/respond.js"></script>
    <![endif]-->

    <?php print $page_bottom; ?>
        
    <div id="copyright" style="visibility: hidden; height: 0; overflow: hidden;">
        <a title="Drupal development agency in Washington DC" target="_blank" href="http://www.inqbation.com">Drupal development agency in Washington DC</a> : <a title="inQbation" target="_blank" href="http://www.inqbation.com">inQbation</a>
    </div>                 

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'CHANGEME-INSERT-GA-ID', 'auto');  
        ga('send', 'pageview');
    </script>
        
</body>
</html>
