<?php


function inqbase_breadcrumb($variables) {

    $output = FALSE;

    $breadcrumb = $variables['breadcrumb'];

    if (!empty($breadcrumb)) {

        $breadcrumb[] = drupal_get_title();

        $output = '<ul class="breadcrumb">';

        foreach ($breadcrumb as $b) {
        	$output .= '<li>'.$b.'</li>';
        }

        $output .= '</ul>';

        return $output;
    }
}


/**
 * Implements hook_js_alter
 * Unset default jQuery so we can add the newest versions
 * @param type $js
 */
function inqbase_js_alter(&$js) {
    unset($js['misc/jquery.js']);
    unset($js['misc/ui/jquery.ui.core.min.js']);
}

/**
 * Implements hook_menu_tree (for user menu only)
 * @global type $level
 * @param type $variables
 * @return type
 */
function inqbase_menu_tree__user_menu($variables) {
	return '<ul class="list-unstyled top-v1-data">' . $variables['tree'] . '</ul>';
}

/**
 * Implemements hook_process_page
 * @param array $variables
 */
function inqbase_preprocess_page(&$variables) {
    
    //Search form
    $search_form = drupal_get_form('search_form');
    $search_form_box = drupal_render($search_form);
    $variables['search_box'] = $search_form_box;

    if ( $views_page = views_get_page_view() ) {
        $variables['theme_hook_suggestions'][] = 'page__views__' . $views_page->name;
        $variables['theme_hook_suggestions'][] = 'page__views__' . $views_page->name . '_' . $views_page->current_display;
    }

    if (isset($variables['node']->type)) {
        $variables['theme_hook_suggestions'][] = 'page__' . $variables['node']->type;
    }

    
    
    // changes the links tree for the main menu, since by
    // default Drupal just provides first level ones
    $menu_tree = menu_tree_all_data('main-menu');
    $menu_tree = menu_tree_output($menu_tree);

    foreach ($menu_tree	as $k=>$v) {
        if (is_numeric($k) && count($v['#below'])) {
            $menu_tree[$k]['#below']['#theme_wrappers'][0] = 'menu_tree__submenu';
        }
    }    
    
    $variables['main_menu'] = $menu_tree;   

}

function inqbase_menu_tree__main_menu($variables) {
    return '<ul class="nav navbar-nav">' . $variables['tree'] . '</ul>';
}

function inqbase_menu_tree__submenu($variables) {
    return '<ul class="dropdown-menu">' . $variables['tree'] . '</ul>';
}

function inqbase_menu_link__main_menu($variables) {

    $element = $variables['element'];
    $sub_menu = '';

    if ($element['#below']) {
        $element['#attributes']['class'][] = 'dropdown';
        $element['#localized_options']['attributes']['class'][] = 'dropdown-toggle';
        $sub_menu = drupal_render($element['#below']);
    }
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

function inqbase_menu_local_tasks(&$variables) {
    $output = '';

    if (!empty($variables['primary'])) {
        $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
        $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs primary">';
        $variables['primary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['primary']);
    }
    if (!empty($variables['secondary'])) {
        $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
        $variables['secondary']['#prefix'] .= '<ul class="nav nav-tabs tabs secondary">';
        $variables['secondary']['#suffix'] = '</ul>';
        $output .= drupal_render($variables['secondary']);
    }

    return $output;
}

/**
* Implements template_preprocess_node()
*/
function inqbase_preprocess_node(&$variables, $hook) {

  /*
  * Search for node type specific functions
  */
  $function = __FUNCTION__ . '_' . $variables['node']->type;
  if (function_exists($function)) {
    $function($variables, $hook);
  }
}

function inqbase_preprocess_views_view_table(&$vars) {
    $vars['classes_array'][] = 'table';
}



function inqbase_item_list($variables) {
    $items = $variables['items'];
    $title = $variables['title'];
    $type = $variables['type'];
    $attributes = $variables['attributes'];

    // Only output the list container and title, if there are any list items.
    // Check to see whether the block title exists before adding a header.
    // Empty headers are not semantic and present accessibility challenges.
    $output = '';
    if (isset($title) && $title !== '') {
        $output .= '<h3>' . $title . '</h3>';
    }

    if (!empty($items)) {
        $output .= "<$type" . drupal_attributes($attributes) . '>';
        $num_items = count($items);
        $i = 0;
        foreach ($items as $item) {
            $attributes = array();
            $children = array();
            $data = '';
            $i++;
            if (is_array($item)) {
                foreach ($item as $key => $value) {
                    if ($key == 'data') {
                        $data = $value;
                    }
                    elseif ($key == 'children') {
                        $children = $value;
                    }
                    else {
                        $attributes[$key] = $value;
                    }
                }
            }
            else {
                $data = $item;
            }
            if (count($children) > 0) {
                // Render nested list.
                $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
            }
            if ($i == 1) {
                $attributes['class'][] = 'first';
            }
            if ($i == $num_items) {
                $attributes['class'][] = 'last';
            }
            $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
        }
        $output .= "</$type>";
    }
    return $output;
}

function inqbase_form_alter(&$form, &$form_state, $form_id) {

    
    // Login Form
    if( $form_id == 'user_login_block' || $form_id == 'user_login' ) {

        $form['name']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-user"></i></span>';
        $form['name']['#field_suffix'] = '</div>'; 
        $form['name']['#description'] = '';
        $form['name']['#attributes']['class'][] = 'form-control';

        $form['pass']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-lock"></i></span>';
        $form['pass']['#field_suffix'] = '</div>'; 
        $form['pass']['#description'] = '';
        $form['pass']['#attributes']['class'][] = 'form-control';

        $form['links']['#markup'] = '<div class="note"><a href="/user/password" title="Request a new password">'.t('Forgot password?').'</a></div>';

        $form['actions']['submit']['#attributes']['class'][] = 'btn-u btn-success pull-right';

    }

    if( $form_id == 'user_pass' ) {

        $form['name']['#field_prefix'] = '<div class="input-group margin-bottom-20"><span class="input-group-addon"><i class="fa fa-user"></i></span>';
        $form['name']['#field_suffix'] = '</div>';
        $form['name']['#description'] = 'You will receive an email notification with more instructions to create a new password.';
        $form['actions']['submit']['#attributes']['class'][] = 'btn-u btn-success';
    }

    if( $form_id == 'user_register_form' ) {
        $form['actions']['submit']['#value'] = t('Register');
    }

    if(isset($form['select']['submit'])){
        $form['select']['submit']['#attributes']['class'][] = 'btn-primary';
    }
}

function print_tabs($tabs, $page, $action_links){      

    //verifying if tabs have edit buttons to render a different skin
    $tabs_style = "tab-v2";
    
    $output_wrapper_open = '';
    $output = '';
    $output_wrapper_close = '';
    
    if(array_key_exists('#primary', $tabs) && !empty($tabs['#primary'])) {
     
        foreach($tabs['#primary'] as $value){
            if( $value['#link']['path'] ==  'node/%/view' || $value['#link']['path'] ==  'user/%/edit') {
                $tabs_style = "tab-v3 box-shadow shadow-effect-1";
            }
        }        
        
    }

    $output_wrapper_open = '<div id="auxiliar-items" class="'. $tabs_style .' margin-bottom-20">';

    if($tabs_style != "tab-v2"):
        $output .= '<div class="navbar-brand">Edit options</div>';
    endif;
    
    if (!empty($tabs)):
        $output .= render($tabs);
    endif;                                                             

    if (!empty($page['help'])):
        $output .= render($page['help']);
    endif;

    if (!empty($action_links)):
        $output .= '<ul class="action-links">'. render($action_links) .'</ul>';
    endif;
    
    $output_wrapper_close .= '</div>';
    
    if( $output == '' ) {
        return '';
    }
    else {
        return $output_wrapper_open . $output . $output_wrapper_close;
    }    
		
}