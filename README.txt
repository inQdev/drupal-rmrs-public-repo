CONTENTS OF THIS FILE
---------------------

 * About the Project
 * How to install
 * Module conventions
 * Theme conventions
 * Branch conventions


ABOUT THE PROJECT
------------------
The Rocky Mountain Research Station (RMRS) is one of five regional units that make up the US Forest Service Research and Development branch. The mission of RMRS, specifically, is to develop and deliver scientific knowledge and technology that will help sustain all of our nation's forests and rangelands.

This project accomplishes the following objectives:
* Make RMRS publications and scientists more discoverable;
* Communicate their science primarily to a land manager and natural resources community audience (including federal, state, and non-profit resource managers, as well as private forest landowners);
* Make their information accessible and relevant to policy makers and local, state, and federal legislators, as well as other academics seeking information about our work and research;
* Demonstrate their value – and remain conceptually accessible and understandable – to members of the general public.

**Requires:** 

* Apache server >= 2 (mod_rewrite active)
* PHP >= 5.3
* MySQL (or MariaDB) >= 5


HOW TO INSTALL
------------------

* Clone the project
* Use initial.sql to create the database
* Copy sites/default/default.settings.php to sites/default/settings.php
* Edit settings.php to match you database configuration
* Ideally, install drush (if not installed yet) http://www.drush.org/en/master/


MODULE CONVENTIONS
--------------------------

Modules, Patches and Drupal Features should be placed inside sites/all/modules

* contrib/ Modules created by the community 
* custom/ Modules created by the company for the project
* patches/ Patches to contrib modules or Drupal core (if needed)
* features/ Drupal Features to handle site configuration


THEME CONVENTIONS
---------------------

The Project has three themes
1. inqbase: Base theme
2. rmrs: Project theme (copy any file from Base theme here in order to override it)
3. seven_inq (Admin theme based on Drupal's Seven theme with project specific configurations


BRANCH CONVENTIONS
--------------------

**Main branch:** master
**Develop branch:** dev

Follow Git Flow Workflow conventions https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow